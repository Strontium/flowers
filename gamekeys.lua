--[[
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
]]


-- Handle keypresses
function handleKeypress(key)
  if key == '1' then gameState = 1
  elseif key == '2' then gameState = 2
  elseif key == '3' then gameState = 3
  end

  if gameState == 3 then
    if key == 'q' and DNAData[DNAPage - 1] ~= nil then
      DNAPage = DNAPage - 1
    elseif key == 'e' and DNAData[DNAPage + 1] ~= nil then
      DNAPage = DNAPage + 1
    end

    if key == "up" then
      if cursorY ~= 1 then cursorY = cursorY - 1 end
    elseif key == "down" then
      if cursorY ~= 3 then cursorY = cursorY + 1 end
    elseif key == "left" then
      if cursorX ~= 1 then cursorX = cursorX - 1 end
    elseif key == "right" then
      if cursorX ~= 4 then cursorX = cursorX + 1 end
    end
  end
end
