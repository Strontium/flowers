--[[
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
]]


function love.conf(t)
  t.window.title     = "Flowers!"
  t.window.resizable = true

  t.window.icon      = "icon.png"

  t.modules.joystick = false
  t.modules.physics  = false
  t.modules.system   = false
  t.modules.thread   = false
end
