--[[
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
]]

-- Localize math
local math = math

local function flowerVisualizer()
  -- Draw sky
  for x = 1, 4 do
    for y = 1, 4 do
      drawPixel(x, y, colors[6][6])
    end
  end

  -- Draw sun
  drawPixel(4, 1, colors[4][math.ceil(environmentSun / 2)])

  -- Draw ground
  for x = 1, 4 do
    drawPixel(x, 3, groundColors[environmentSoil])
  end

  -- Draw rain
  if raining then
    drawPixel(3, rainY, colors[6][4])
  end
end

local function flowerStatuses()
  -- Draw hydration bar
  for x = 1, math.ceil(flowerHydration/2) do
    drawPixel(x, 1, colors[6][x])
  end

  -- Draw nutrients bar
  for x = 1, math.ceil(flowerNutrients/2) do
    drawPixel(x, 2, colors[3][x])
  end

  -- Draw sunlight bar
  for x = 1, math.ceil(flowerSunlight/2) do
    drawPixel(x, 3, colors[4][x])
  end
end

local function DNAEditor()
  -- Display top row of DNA
  for x = 1, 4 do
    drawPixel(x, 1, DNAColors[DNAData[DNAPage][1][x]])
  end

  -- Display bottom row of DNA
  for x = 1, 4 do
    drawPixel(x, 2, DNAColors[DNAData[DNAPage][2][x]])
  end

  -- Draw cursor
  if cursorBlink then
    drawPixel(cursorX, cursorY, colors[6][1])
  end
end

gameStates = {
  flowerVisualizer,
  flowerStatuses,
  DNAEditor,
}
