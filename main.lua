--[[
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
]]

-- Flowers, by Malcolm Forde

require "utilities"
require "colors"
require "gamekeys"
require "gamestates"
require "gameloop"

-- This file is just for LOVE stuff. For the real stuff, check out gamestates.lua, gamekeys.lua and gametick.lua

function love.load()
  -- Flower status
  flowerHydration  = 8
  flowerNutrients  = 8
  flowerSunlight   = 8

  flowerHeat       = 8
  flowerHealth     = 8

  -- DNA
  DNAPage          = 1
  DNAData          = {
    {
      {1, 2, 3, 4},
      {2, 1, 4, 3},
    }, {
      {2, 3, 1, 4},
      {1, 4, 2, 3},
    },
  }
  -- Environment status
  environmentWater = 8
  environmentSoil  = 8
  environmentSun   = 8

  -- Game state
  gameState        = 1

  -- Cursor info
  lastBlink   = 0
  cursorBlink = true
  cursorX     = 1
  cursorY     = 1

  -- Rain stuff
  timeSinceDrop   = 0
  raining         = false
  rainY           = 1
  rainingSince    = 0
  lastRain        = 0
  rainChance      = 5
  rainFreq        = 15
  maxRainDuration = 10

  -- Get the size of each pixel
  pixelWidth, pixelHeight = getPixelSize(4, 3)
end

function love.resize()
  -- Get the size of each pixel
  pixelWidth, pixelHeight = getPixelSize(4, 3)
end

function love.keypressed(key)
  handleKeypress(key)
end

function love.update(dt)
  loop(dt)
end

function love.draw()
  gameStates[gameState]()
  love.graphics.setColor(HSL(0, 255, 255))
  love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)
end
