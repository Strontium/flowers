--[[
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
]]


function loop(dt)
  -- Random stuff, see?
  RandomGenerator = love.math.newRandomGenerator()

  -- Handle cursor blinking
  lastBlink = lastBlink + dt

  if lastBlink > 0.25 then
    cursorBlink = not cursorBlink
    lastBlink = 0
  end

  if not raining and lastRain > rainFreq and RandomGenerator:random(1, 100) > rainChance then
    raining = not raining
    lastRain = 0
  end

  if not raining then
    lastRain = lastRain + dt
  end

  if raining then
    timeSinceDrop = timeSinceDrop + dt
    if timeSinceDrop > 0.25 then
      if rainY ~= 3 then rainY = rainY + 1
      else rainY = 1 end
      timeSinceDrop = 0
    end
    rainingSince = rainingSince + dt
  end

  if rainingSince > maxRainDuration then
    raining = not raining
    rainingSince = 0
  end
end
