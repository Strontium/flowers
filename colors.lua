--[[
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
]]

-- Color palette
colors = {
  -- Reddish white/grey
  {
    {HSL(  0,  20,  80)},
    {HSL(  0,  20, 100)},
    {HSL(  0,  20, 120)},
    {HSL(  0,  20, 140)},
    {HSL(  0,  20, 160)},
    {HSL(  0,  20, 180)},
    {HSL(  0,  20, 200)},
    {HSL(  0,  20, 220)}
  },
  -- Red
  {
    {HSL(  0, 200,  80)},
    {HSL(  0, 200, 100)},
    {HSL(  0, 200, 120)},
    {HSL(  0, 200, 140)},
    {HSL(  0, 200, 160)},
    {HSL(  0, 200, 180)},
    {HSL(  0, 200, 200)},
    {HSL(  0, 200, 220)}
  },
  -- Brown
  {
    {HSL( 15, 200,  80)},
    {HSL( 15, 200, 100)},
    {HSL( 15, 200, 120)},
    {HSL( 15, 200, 140)},
    {HSL( 15, 200, 160)},
    {HSL( 15, 200, 180)},
    {HSL( 15, 200, 200)},
    {HSL( 15, 200, 220)}
  },
  -- Yellow
  {
    {HSL( 30, 200,  80)},
    {HSL( 30, 200, 100)},
    {HSL( 30, 200, 120)},
    {HSL( 30, 200, 140)},
    {HSL( 30, 200, 160)},
    {HSL( 30, 200, 180)},
    {HSL( 30, 200, 200)},
    {HSL( 30, 200, 220)}
  },
  -- Green
  {
    {HSL(100, 200,  80)},
    {HSL(100, 200, 100)},
    {HSL(100, 200, 120)},
    {HSL(100, 200, 140)},
    {HSL(100, 200, 160)},
    {HSL(100, 200, 180)},
    {HSL(100, 200, 200)},
    {HSL(100, 200, 220)}
  },
  -- Blue
  {
    {HSL(140, 200,  80)},
    {HSL(140, 200, 100)},
    {HSL(140, 200, 120)},
    {HSL(140, 200, 140)},
    {HSL(140, 200, 160)},
    {HSL(140, 200, 180)},
    {HSL(140, 200, 200)},
    {HSL(140, 200, 220)}
  }
}

-- Ground colors, for simplicity
groundColors = {
  colors[3][4],
  colors[3][3],
  colors[3][2],
  colors[3][1],
  colors[5][4],
  colors[5][3],
  colors[5][2],
  colors[5][1]
}

-- DNA colors, for simplicity
DNAColors = {
  colors[2][3],
  colors[5][3],
  colors[4][3],
  colors[6][3],
}
