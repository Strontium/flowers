-- I don't know how to license this since it uses someone elses from under another license. How do I do this? :P

-- Converts HSL to RGB. (input and output range: 0 - 255) (from love2d.org/wiki/HSL_color)
function HSL(h, s, l, a)
  if s<=0 then return l,l,l,a end
  h, s, l = h/256*6, s/255, l/255
  local c = (1-math.abs(2*l-1))*s
  local x = (1-math.abs(h%2-1))*c
  local m,r,g,b = (l-.5*c), 0,0,0
  if h < 1     then r,g,b = c,x,0
  elseif h < 2 then r,g,b = x,c,0
  elseif h < 3 then r,g,b = 0,c,x
  elseif h < 4 then r,g,b = 0,x,c
  elseif h < 5 then r,g,b = x,0,c
  else              r,g,b = c,0,x
  end return (r+m)*255,(g+m)*255,(b+m)*255,a
end

-- Get how big each pixel should be
function getPixelSize(gameResolutionX, gameResolutionY)
  local windowWidth  = love.graphics.getWidth()
  local windowHeight = love.graphics.getHeight()

  local pixelWidth   = math.ceil(windowWidth / gameResolutionX)
  local pixelHeight  = math.ceil(windowHeight / gameResolutionY)

  return pixelWidth, pixelHeight
end

-- Draw a pixel
function drawPixel(x, y, r, g, b)
  local x = x - 1
  local y = y - 1

  -- Get real location of each pixel
  local realX = x * pixelWidth
  local realY = y * pixelHeight

  -- Draw pixel
  love.graphics.setColor(r, g, b)
  love.graphics.rectangle("fill", realX, realY, pixelWidth, pixelHeight)
end
